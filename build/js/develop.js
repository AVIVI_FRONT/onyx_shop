function dataDropdown(){
    var btn = '[data-dropdown-btn]';
    if(btn.length){
        $(document).on('click', btn, clickHandler);
    }else{
        $(document).off('click', btn, clickHandler);
    }
    function clickHandler(){
        var id = $(this).attr('data-dropdown-btn');
        if(id.length === 0 || id === undefined){
            $(this).toggleClass('active');
            $(this).next().toggleClass('active');
        } else if(!$(this).hasClass('active')){
            openDD(id);
        }else{
            closeDD(id);
        }
    }
    var close = '[data-dropdown-close]';
    if(close.length){
        $(document).on('click', close, closeHandler);
    }else{
        $(document).off('click', close, closeHandler);
    }
    function closeHandler(){
        var id = $(this).attr('data-dropdown-close');
        if(!$(this).hasClass('active')){
            closeDD(id);
        }
    }
}
function openDD(id) {
    $('[data-dropdown-box]').removeClass('active');
    $('[data-dropdown-btn]').removeClass('active');
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.addClass('active');
    box.addClass('active');
}
function closeDD(id) {
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.removeClass('active');
    box.removeClass('active');
}

function mainSliderInit() {
    var slider = $('.js-main-slider');
    if (slider) {
        slider.slick({
            dots: false,
            fade: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 5000,
            swipe: true,
            prevArrow: '<button class="js-go-back arrow" tabindex="0"> <svg> <use xlink:href="#prev"></use> </svg> </button>',
            nextArrow: '<button class="js-go-forward arrow arrow--rt" tabindex="0"> <svg> <use xlink:href="#next"></use> </svg> </button>',
            appendArrows: $('.slider__arrows')
        });
        $(document).on('click', '.js-go-back', function () {
            slider.slick('slickPrev');
        });
        $(document).on('click', '.js-go-forward', function () {
            slider.slick('slickNext');
        })
    }
}

function instaSlider() {
    var slider = $('.js-insta-slider');
    if (slider) {
        slider.slick({
            dots: false,
            arrows: false,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 1110,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 860,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
    $(document).on('click', '.js-insta-back', function () {
        slider.slick('slickPrev');
    });
    $(document).on('click', '.js-insta-fwd', function () {
        slider.slick('slickNext');
    });
}

function showSeo() {
    $(document).on('click', '.js-show-seo', function () {
        var button = $(this);
        var actual_height = $(this).parents('.seo').find('.seo__text').height();
        console.log(actual_height);
        var field = button.parents('.seo').find('.seo__wrap');
        if (button.hasClass('active')) {
            button.removeClass('active');
            field.removeClass('active').css('max-height', 75);
        } else {
            button.addClass('active');
            field.addClass('active').css('max-height', actual_height + 10);
        }
    })
}

function showSearch() {
    $(document).on('click', '.search__btn', function () {
        var button = $(this);
        var inp = $(this).parents('.search');
        if (button.hasClass('active')) {
            button.removeClass('active');
            inp.removeClass('active');
        } else {
            button.addClass('active');
            inp.addClass('active');
        }
    })
}


function showCart() {
    $(document).on('click', '.stash > .stash__button', function (e) {
        e.preventDefault();
        var button = $(this).parents('.stash');
        var inp = button.find('.stash__pop');
        if (button.hasClass('active')) {
            button.removeClass('active');
            inp.removeClass('active');
        } else {
            button.addClass('active');
            inp.addClass('active');
        }
    })
}

function showMenu() {
    $(document).on('click', '.js-menu-caller', function () {
        var height = $('header').outerHeight();
        var menu = $('.menu__wrapper');
        var button = $(this);
        if (button.hasClass('active')) {
            button.removeClass('active');
            menu.removeClass('active');
        } else {
            button.addClass('active');
            menu.addClass('active');
        }
    })
}

function showFooterInfo() {
    $(document).on('click', '.footer__button', function (e) {
        e.preventDefault();
        var btn = $(this);
        var field = $('.footer__info');
        if (btn.hasClass('active')) {
            btn.removeClass('active');
            field.removeClass('active');
        } else {
            btn.addClass('active');
            field.addClass('active');
        }
        $('.footer_placeholder').height($('.footer').outerHeight());
    })
}

function setCookie(name, value, expires) {
    var options = {};
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*60*60*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
    options.path = '\/';
    // options.domain = 'mono.devplace.info';
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    updatedCookie += ";" ;
    document.cookie = updatedCookie;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}

function closeBanner() {
    if(getCookie("header__banner") != -1)
    {
        $('.header__banner').show();
        checkTheHeader();
        $(document).on('click', '.banner__close', function () {
            $(this).parents('.header__banner').slideUp('fast');
            setCookie("header__banner", -1, 1);
            setTimeout(function () {
                checkTheHeader();
            }, 300);
            return false;
        });
    }
}

function menuLevels() {
    $(document).on('click', '.js-hasnext', function (e) {
        if ($('.upper__sticks').css('display') === 'block') {
            e.preventDefault();
        }
        $(this).parents('li').find('.nextlevel').addClass('active');

    });
    $(document).on('click', '.backmenu', function () {
        $(this).parents('.nextlevel').removeClass('active');
    })
}

function closeCatalog() {
    $(document).on('click', '.js-close-catalog', function () {
        $('.js-filters').removeClass('active');
        $('.js-show-filters').removeClass('active');
        $('.filter-show').remove();
    })
}

function tabChanger() {
    $(document).on('click', '.tabs__link', function (e) {
        e.preventDefault();
        var current = $(this);
        var number = $(this).index();
        var item = $(this).parents('.tabs').find('.tabs__item');
        if (current.hasClass('active')) {
            return false;
        } else {
            current.parents('.tabs').find('.tabs__link').removeClass('active');
            current.addClass('active');
            item.removeClass('active');
            item.eq(number).addClass('active');
        }
    })
}

function gallerySliderInit() {
    var small_slider = $('.js-small-slider');
    var back = '<button class="arrow arrow--white arrow--left js-small-back"><svg><use xlink:href="#prev"></use></svg></button>';
    var forward = '<button class="arrow arrow--white arrow--rt arrow--right js-small-fwd"><svg><use xlink:href="#next"></use></svg></button>';
    if (small_slider) {
        small_slider.slick({
            dots: false,
            slidesToShow: 10,
            asNavFor: '.js-big-slider',
            slidesToScroll: 1,
            infinite: true,
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 9,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1110,
                    settings: {
                        slidesToShow: 7
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 460,
                    settings: {
                        slidesToShow: 3
                    }
                }
            ]
        });
        var active_slide = small_slider.find('.gallery__slide.slick-active');
        active_slide.first().find('.gallery__info').prepend(back).addClass('shadow');
        active_slide.eq(active_slide.length - 1).find('.gallery__info').prepend(forward).addClass('shadow');
    }
    $(document).on('click', '.js-small-back', function () {
        small_slider.slick('slickPrev');
    });
    $(document).on('click', '.js-small-fwd', function () {
        small_slider.slick('slickNext');
    });
    var big_slider = $('.js-big-slider');
    if (big_slider) {
        big_slider.slick({
            dots: false,
            slidesToShow: 1,
            asNavFor: '.js-small-slider',
            infinite: true
        });
    }
    small_slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var slide =  small_slider.find('.gallery__slide');
        slide.find('.shadow').removeClass('shadow');
        slide.find('.arrow').remove();
    });
    small_slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
        var need_slide = small_slider.find('.gallery__slide.slick-active');
        need_slide.first().find('.gallery__info').prepend(back).addClass('shadow');
        need_slide.eq(need_slide.length - 1).find('.gallery__info').prepend(forward).addClass('shadow');
    });
}



function halfSliderInit() {
    var small_slider = $('.js-small-slider-half');
    var back = '<button class="arrow arrow--white js-small-back-half"><svg><use xlink:href="#prev"></use></svg></button>';
    var forward = '<button class="arrow arrow--white arrow--rt js-small-fwd-half"><svg><use xlink:href="#next"></use></svg></button>';
    if (small_slider) {
        small_slider.slick({
            dots: false,
            slidesToShow: 5,
            asNavFor: '.js-big-slider-half',
            slidesToScroll: 1,
            arrows: false,
            infinite: true,
            vertical: true,
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1110,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 600,

                    settings: {
                        slidesToShow: 5,
                        vertical: false
                    }
                },
                {
                    breakpoint: 460,
                    settings: {
                        vertical: false,
                        slidesToShow: 5
                    }
                }
            ]
        });
        var active_slide = small_slider.find('.gallery__slide.slick-active');
        active_slide.first().find('.gallery__info').prepend(back).addClass('shadow');
        active_slide.eq(active_slide.length - 1).find('.gallery__info').prepend(forward).addClass('shadow');
    }
    $(document).on('click', '.js-small-back-half', function () {
        small_slider.slick('slickPrev');
    });
    $(document).on('click', '.js-small-fwd-half', function () {
        small_slider.slick('slickNext');
    });
    var big_slider = $('.js-big-slider-half');
    if (big_slider) {
        big_slider.slick({
            dots: false,
            slidesToShow: 1,
            asNavFor: '.js-small-slider-half',
            infinite: true
        });
    }
    small_slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var slide =  small_slider.find('.gallery__slide');
        slide.find('.shadow').removeClass('shadow');
        slide.find('.arrow').remove();
    });
    small_slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
        var need_slide = small_slider.find('.gallery__slide.slick-active');
        need_slide.first().find('.gallery__info').prepend(back).addClass('shadow');
        need_slide.eq(need_slide.length - 1).find('.gallery__info').prepend(forward).addClass('shadow');
    });
}

// function stylerInit() {
//     $('select.select').styler({
//         selectSmartPositioning: false
//     });
// }

function watchedSlider() {
    var slider = $('.js-watched-slider');
    if (slider) {
        slider.slick({
            dots: false,
            slidesToShow: 4,
            slidesToScroll: 1,

            prevArrow: '<button class="arrow arrow--grey arrow--left"><svg><use xlink:href="#prev"></use></svg></button>',
            nextArrow: '<button class="arrow arrow--grey arrow--rt arrow--right"><svg><use xlink:href="#next"></use></svg></button>',
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 3
                        // centerMode: true
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 410,
                    settings: {
                        slidesToShow: 1
                        // centerMode: true
                    }
                }

            ]
        })
    }
}

function hiderFilter() {
    $(document).on('click', '.hider__head', function () {
        var button = $(this);
        var field = $(this).parents('.hider').find('.hider__body');
        if (button.hasClass('active')) {
            button.removeClass('active');
            field.removeClass('active');
        } else {
            button.addClass('active');
            field.addClass('active');
        }
    });
}
function hideFilter(){
     $('.js-show-filters').removeClass('active');
     $('.js-filters').removeClass('active');
     $('.filter-show').remove();
}
function showFilters() {
    $(document).on('click', function (event) {
        var button = $('.js-show-filters');
        var filters = $('.js-filters');
        if(!button.is(event.target) && button.has(event.target).length === 0 && !filters.is(event.target) && filters.has(event.target).length === 0 ) {
           hideFilter();
        }
    });

    $(document).on('click', '.js-show-filters', function () {
        var button = $(this);
        var filters = $('.js-filters');
        if (button.hasClass('active')) {
            hideFilter();

        } else {
            button.addClass('active');
            filters.addClass('active');
        }
    });
}

function hiLowPrice() {
    $(document).on('click', '.js-hi-low', function () {
        var button = $(this);
        if (button.hasClass('first') && button.hasClass('second')) {
            button.removeClass('second');
        } else  if(button.hasClass('first') && !button.hasClass('second')) {
            button.addClass('second');
        } else {
            button.addClass('first');
        }
    });
}

function filterButtonsCheck() {
    $(document).on('click', '.js-filter-btn', function () {
        var btn = $(this);
        if (btn.hasClass('active')) {
            btn.removeClass('active');
        } else {
            btn.addClass('active');
        }
    });
}



function headChecker() {
    var header = $('.header');
    var filter = $('.beforepage');
    $(window).on('scroll', function () {
        var height = $(window).scrollTop();
        if (height > 280) {
            header.addClass('fixed');
            if (filter) {
                filter.addClass('fixed');
            }
            $('.main').addClass('lawl');
            // content.css('padding-top', head_height);
        } else {
            header.removeClass('fixed');
            if (filter) {
                filter.removeClass('fixed');
            }
            $('.main').removeClass('lawl');
            // content.css('padding-top', 0);
        }
    });
}

function checkTheHeader() {
    var header_height = $('header').outerHeight();
    var filter_height = $('.beforepage').outerHeight();
    $('.main').css('padding-top', header_height);
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
function filterApply(){
    $(document).on('change', '.filters__checks input', function(){
        var y = $(this).next('.btn').length > 0 ? $(this).offset().top + 12 : $(this).offset().top;
        var x = $(this).offset().left;
        $('.filter-show').remove();
        var popup = document.createElement('div');
        popup.classList.add('filter-show');
        var html = "<button type='button'>показать 42</button> ";
        $(popup).append(html).css({'top':y - 10, 'left': x + 260});
        $('body').append(popup);
    });
}

var myEfficientFn = debounce(function() {
    checkTheHeader();
}, 100);

function setCursors() {
    $(document).on('click', '.tel-mask', function () {
        $(this).setCursorPosition(1);
    });
    // $('.tel-mask').keyup(function () {
    //     var value = $(this).val();
    //     var spl_val = value.split('');
    //     var pos = spl_val.indexOf("_");
    //     console.log(pos);
    //     console.log($(this).val());
    //     $(this).setCursorPosition(pos);
    // });
}

$.fn.setCursorPosition = function(pos) {
    this.each(function(index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

function selectTwoInit() {
    $('.js-towns-select').select2({
        width: 'style'
    });
    $('.js-post-select').select2({
        width: 'style'
    });
}

window.addEventListener('resize', myEfficientFn);


$('[data-fancybox]').fancybox({
    buttons: [
        "close"
    ],
    beforeShow : function ( instance, current ) {
        if ($('.fancybox-navigation').hasClass('stop')) {
            return false;
        } else {
            $('.fancybox-button--arrow_right').css('opacity', '0');
            $('.fancybox-button--arrow_left').css('opacity', '0');
        }
    },
    afterShow : function( instance, current ) {
        var window_width = $(window).width();
        var content_width = $('.fancybox-content').width();
        var right_arrow = $('.fancybox-button--arrow_right');
        var left_arrow = $('.fancybox-button--arrow_left');
        $('.fancybox-navigation').addClass('stop');
        right_arrow.css('right', 'calc(50% - ' + (content_width/2 + 70) + 'px)');
        left_arrow.css('left', 'calc(50% - ' + (content_width/2 + 70) + 'px)');
        right_arrow.css('opacity', '');
        left_arrow.css('opacity', '');
    }
});

function changeArrowPlace() {
    var window_width = $(window).width();
    var content_width = $('.fancybox-content').width();
    $('.fancybox-button--arrow_right').css('right', 'calc(50% - ' + (content_width/2 + 70) + 'px)');
    $('.fancybox-button--arrow_left').css('left', 'calc(50% - ' + (content_width/2 + 70) + 'px)');
}

$(window).on('resize',function () {
   changeArrowPlace();
});



$(document).ready(function () {
    // filterApply();
    $('input,textarea').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
            .attr('placeholder','');
    }).blur(function(){
        $(this).attr('placeholder',$(this).data('placeholder'));
    });

    selectTwoInit();
    setCursors();
    checkTheHeader();
    filterButtonsCheck();
    hiLowPrice();
    showCart();
    showFilters();
    hiderFilter();
    watchedSlider();
    // stylerInit();
    halfSliderInit();
    gallerySliderInit();
    tabChanger();
    closeCatalog();
    menuLevels();
    closeBanner();
    showFooterInfo();
    showMenu();
    headChecker();
    showSearch();
    showSeo();
    mainSliderInit();
    instaSlider();
    dataDropdown();

    if($('.beforepage__select select').length){
        $('.beforepage__select select').styler({selectSmartPositioning: false});
    }
});