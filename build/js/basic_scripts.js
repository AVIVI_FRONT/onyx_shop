jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

var scroller=jQuery.browser.webkit ? "body": "html";

$.scrollbarWidth=function(){var a,b,c;if(c===undefined){a=$('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');b=a.children();c=b.innerWidth()-b.height(99).innerWidth();a.remove()}return c};
function slidersInit() {
    var el = $('.js-card__itemslider');
    if(el.length){
        el.slick({
            dots:false,
            nextArrow: '<button class="slick__next slick__arrow"><svg><use xlink:href="#next"></use></svg></button>',
            prevArrow: '<button class="slick__prev slick__arrow"><svg><use xlink:href="#prev"></use></svg></button>',
        });
    }
}
var lets_go = true;
function parampicker() {
    if (lets_go) {
        var els = $('.param');
        if(els.length > 0){
            els.each(function () {

                var box = $(this);

                var width = 0;
                var pins = box.find('.param__item');
                pins.each(function(){
                    var w = $(this).find('label').width();
                    if (w > width) width = w;
                });
                box.find('.param__body').width(width + 90);

                box.find('.param__next').click(function () {
                    box.find('.param__button').removeClass('disabled');
                    box.find('.active.param__item').removeClass('active').removeClass('next').next().addClass('active next').click();
                    if(box.find('.active.param__item').index() > pins.length - 2) $(this).addClass('disabled');
                    box.find('.active.param__item').find('input').prop('checked', true).trigger('change');
                });

                box.find('.param__prev').click(function () {
                    box.find('.param__button').removeClass('disabled');
                    box.find('.param__item').removeClass('next');
                    box.find('.active.param__item').removeClass('active').prev().addClass('active').click();
                    if(box.find('.active.param__item').index() == 0) $(this).addClass('disabled');
                    box.find('.active.param__item').find('input').prop('checked', true).trigger('change');
                });
            })
        }
    }
    lets_go = false;
}
function jsRadioGroup() {
    var input =  $('.js-radio-group input');
    if ( input.length > 0 ){
        input.on('change', function(){
            $(this).closest('.js-radio-group').find('.basket__input').addClass('hide');
            $(this).closest('label').next('.basket__input').removeClass('hide');
        });
    }
}
$(document).ready(function() {
    slidersInit();
    parampicker();
    jsRadioGroup();
});

(function(window, document) {
    'use strict';


    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) return true;
    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        request,
        data,
        insertIT = function() {
            document.body.insertAdjacentHTML('afterbegin', data);
        },
        insert = function() {
            if (document.body) insertIT();
            else document.addEventListener('DOMContentLoaded', insertIT);
        };
    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }
    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrev', revision);
                }

            }
        }
        request.send();
    } catch (e) {}
}(window, document));